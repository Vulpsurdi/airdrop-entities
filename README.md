# Airdrop Plane

This repository contains a couple of entities and a generic lua file for spawning an airplane that drops a package at a random location on a map. I wrote this as part of a gamemode called GmodZ for a server ran by the XMP Studios community but this never saw the light of day since I lost interest and stopped working on it. Due to interest in the plane sound effect I'm releasing the relevant code here. Since these files were pulled from a gamemode they won't work on their own but it shouldn't take much work to repurpose the code for use in other projects.

The `sv_airdrop.lua` file uses coordinates from the `rp_stalker_final` map which no longer seems to be on Workshop but any of the rp_stalker maps should be similar enough.

The jet model comes from this Sakarias88's Air Vehicles workshop addon: https://steamcommunity.com/sharedfiles/filedetails/?id=104506205
