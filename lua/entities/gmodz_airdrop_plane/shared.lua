
ENT.Base        = "base_anim"
ENT.PrintName   = "Airdrop Plane"
ENT.Author      = "Ronny"
ENT.Information = "Passes overhead dropping airdrop cargo."
ENT.Category    = "GmodZ"

ENT.Editable    = false
ENT.Spawnable   = false
ENT.AdminOnly   = false
ENT.RenderGroup = RENDERGROUP_OPAQUE

