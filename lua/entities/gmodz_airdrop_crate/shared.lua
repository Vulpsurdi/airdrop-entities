
ENT.Base        = "gmodz_lootable"
ENT.PrintName   = "Airdrop Crate"
ENT.Author      = "Ronny"
ENT.Information = "Dropped by airdrop planes."
ENT.Category    = "GmodZ"

ENT.Editable    = false
ENT.Spawnable   = false
ENT.AdminOnly   = true
ENT.RenderGroup = RENDERGROUP_OPAQUE

