
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

DEFINE_BASECLASS("gmodz_lootable")

local mdlCrate = Model("models/props/cs_militia/crate_extrasmallmill.mdl")

function ENT:Initialize()

    BaseClass.Initialize(self, mdlCrate)

    self:SetSolid(SOLID_VPHYSICS)
    self:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)
    self:SetMoveType(MOVETYPE_VPHYSICS)

    local phys = self:GetPhysicsObject()
    if phys:IsValid() then
        phys:Wake()
    end
end

function ENT:PhysicsUpdate(physobj)
    local force = math.min(-self:GetVelocity().z*20, 0)
    physobj:ApplyForceCenter(Vector(0, 0, force))
end


