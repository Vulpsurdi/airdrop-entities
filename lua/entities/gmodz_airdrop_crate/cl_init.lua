
include('shared.lua')

local mdlChute = Model("models/props_phx/construct/metal_dome360.mdl")

DEFINE_BASECLASS("gmodz_lootable")

function ENT:Initialize()
    BaseClass.Initialize(self)

    self.chute = ents.CreateClientProp()
    self.chute:SetModel(mdlChute)
    self.chute:Spawn()
end

function ENT:Think()
    BaseClass.Think(self)

    if not IsValid(self.chute) then return end

    local vel = self:GetVelocity()
    local speed = vel:Length()

    self.chute:SetPos(self:LocalToWorld(self:OBBCenter()) - vel*0.1)
    self.chute:SetAngles(vel:Angle() + Angle(-90, 0, 0))

    local scale = math.min(speed/(self.scale or 600), 1)
    local matrix = Matrix()
    matrix:Scale(Vector(scale, scale, scale*0.8))
    self.chute:EnableMatrix("RenderMultiply", matrix)

    self.chute:SetNoDraw(speed < 400)
end

function ENT:OnRemove()
    if IsValid(self.chute) then
        self.chute:Remove()
    end

    BaseClass.OnRemove(self)
end

function ENT:Draw()
    self:DrawModel(true)
    self:DrawShadow(true)
end

