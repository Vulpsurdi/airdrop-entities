
gmodz.airdrop = gmodz.airdrop or {}
local airdrop = gmodz.airdrop

local locations = {
    Vector(1861, 13114, 159),    -- Original
    Vector(-13687, -9827, -647), -- Military
    Vector(6484, 2042, -623),    -- Airfield
    Vector(-5498, 10545, -510),  -- Pipes by APC
    Vector(11482, -2920, -519),  -- College / electrical tower
    Vector(-412, 1530, 80),      -- School roof
    Vector(-6441, 2407, -613),   -- Swamp ship
}

local centerX -- X-coordinate center of the map.
local centerY -- Y-coordinate center of the map
local angQ4   -- Angle for quadrant iv corner.
local angQ1   -- Angle for quadrant i corner.
local angQ2   -- Angle for quadrant ii corner.
local angQ3   -- Angle for quadrant iii corner.

--------------------------------------------------------------------------------

--[[ Get a map boundary coordinate by projecting a line from the origin of the
     map towards the map's boundaries.
     @param radians - Direction from the center of the map.
     @returns point - The map boundary coordinate. ]]
function airdrop.Point(radians)
    local altitude = gmodz.airdropAltitude
    local xMin = gmodz.mapXMin
    local xMax = gmodz.mapXMax
    local yMin = gmodz.mapYMin
    local yMax = gmodz.mapYMax

    radians = radians % (math.pi*2)

    -- Get the point on the unit circle.
    local unitX = math.cos(radians)
    local unitY = math.sin(radians)

    -- Calculate the map center and unit angles for each map corner.
    -- This only needs to be done once.
    if not centerX then
        centerX = (xMin + xMax)/2
        centerY = (yMin + yMax)/2

        angQ1 = math.atan((yMax-centerY)/(xMax-centerX))
        angQ2 = math.atan((yMax-centerY)/(xMin-centerX)) + math.pi
        angQ3 = math.atan((yMin-centerY)/(xMin-centerX)) + math.pi
        angQ4 = math.atan((yMin-centerY)/(xMax-centerX)) + math.pi*2
    end

    local ratio
    if radians >= angQ1 and radians <= angQ2 then
        -- Positive Y
        ratio = yMax/unitY
    elseif radians >= angQ2 and radians <= angQ3 then
        -- Negative X
        ratio = xMin/unitX
    elseif radians >= angQ3 and radians <= angQ4 then
        -- Negative Y
        ratio = yMin/unitY
    else
        -- Positive X
        ratio = xMax/unitX
    end

    -- Scale point to be on the map boundary edge.
    local x = unitX * ratio
    local y = unitY * ratio

    -- Clamp just to be safe.
    x = math.Clamp(x, xMin, xMax)
    y = math.Clamp(y, yMin, yMax)

    return Vector(x, y, altitude)
end

--[[ Construct a path across the map using two angles.
     @param radiansx - Start angle.
     @param radiansy - End angle.
     @returns pointx, pointy - The start and end coordinates for the path. ]]
function airdrop.Path(radiansx, radiansy)
    return airdrop.Point(radiansx), airdrop.Point(radiansy)
end

--[[ Construct a random path across the map.
     @returns pointx, pointy - The start and end coordinates for the path. ]]
function airdrop.RandomPath()
    local a = math.random() * 2*math.pi
    local b = a + 110 + math.random()*140
    return airdrop.Path(a, b)
end

--------------------------------------------------------------------------------

-- For testing: starts at bunker and crosses through town.
-- gmodz.CallAirdrop(Vector(449, -14386, 4000), Vector(641, 11297, 4000))
function airdrop.SpawnPlane(from, to)
    if not from then
        from, to = airdrop.RandomPath()
    end

    local ent = ents.Create("gmodz_airdrop_plane")
    ent:Spawn()
    ent:SetCourse(from, to)

    gmodz.Log("Airdrop spawned.")
    PrintMessage(HUD_PRINTTALK, "An airdrop is inbound!")
end

function airdrop.SpawnPackage(plane)
    local invid = gmodz.inv.Create("airdrop", 20)
    airdrop.PopulatePackage(invid)

    local package = ents.Create("gmodz_airdrop_crate")
    package:SetPos(plane:GetPos())
    package:Spawn()
    package:SetInventory(invid)
    constraint.NoCollide(package, plane, 0, 0)

    local physobj = package:GetPhysicsObject()
    if IsValid(physobj) then
        physobj:SetVelocity(plane:GetVelocity()*0.7)
    end
end

--------------------------------------------------------------------------------

local nextThink
local lastAirdrop
function airdrop.Think()
    if nextThink and CurTime() < nextThink then return end
    nextThink = CurTime() + 60

    if player.GetCount() < gmodz.gmodz_airdrop_minplayers:GetFloat() then
        return
    end

    local interval = gmodz.gmodz_airdrop_interval:GetFloat()
    if lastAirdrop and CurTime() < lastAirdrop + interval then return end

    local ratePerMinute = gmodz.gmodz_airdrop_rateperhour:GetFloat() / 60
    if math.Rand(0, 1) > ratePerMinute then return end

    lastAirdrop = CurTime()
    airdrop.SpawnPlane()
end

--------------------------------------------------------------------------------

concommand.Add("gmodz_airdrop_spawn", function(ply)
    if not ply:IsSuperAdmin() then return end
    airdrop.SpawnPlane()
    ply:PrintMessage(HUD_PRINTCONSOLE, "Airdrop spawned.")
    ply:Log("Spawned an airdrop.")
end)

